// Question 1 -------------------------------------------------------------------

function matchPlayed(matches) {
  return matches
    .map((match) => match.season)
    .reduce((result, season) => {
      if (result[season]) {
        result[season]++;
      } else {
        result[season] = 1;
      }
      return result;
    }, {});
}

// Question 2 ---------------------------------------------------------------------------------------------------

function matchWonbyTeam(matches) {
  return matches.reduce((result, year) => {
    let season = year.season;
    let winner = year.winner;

    if (!result[season]) {
      result[season] = {};
    } else {
      result[season][winner] = result[season][winner] + 1 || 1;
    }

    return result;
  }, {});
}

// // Question 3 ---------------------------------------------------------------------------------------------------------

function extraRunsbyPerTeam(matches, deliveries, year) {
  let id = [];
  matches.filter((match) => {
    if (match.season.includes(year)) id.push(match.id);
  });

  let ans = deliveries.reduce((result, delivery) => {
    if (id.includes(delivery.match_id)) {
      if (!result[delivery.bowling_team]) {
        result[delivery.bowling_team] = parseInt(delivery.extra_runs);
      } else {
        result[delivery.bowling_team] += parseInt(delivery.extra_runs);
      }
    }
    return result;
  }, {});

  return ans;
}

// // Question 4 ---------------------------------------------------------------------------------------------------------

function economyBowler(matches, deliveries, year) {
  let id = [];
  matches.filter((match) => {
    if (match.season.includes(year)) id.push(match.id);
  });

  // return id

  function countOver(bowler) {
    let over = deliveries.reduce((balls, delivery) => {
      if (id.includes(delivery.match_id)) {
        if (delivery.bowler == bowler) {
          balls += 1;
        }
      }
      return +balls;
    }, 0);

    return over / 6;
  }

  let bowlerRuns = deliveries.reduce((result, delivery) => {
    if (id.includes(delivery.match_id)) {
      if (result[delivery.bowler]) {
        result[delivery.bowler] += parseInt(delivery.total_runs);
      } else {
        result[delivery.bowler] = parseInt(delivery.total_runs);
      }
    }
    return result;
  }, {});

  // return a

  bowlerRuns = Object.entries(bowlerRuns)
    .map((key) => [key[0], Math.round(key[1] / countOver(key[0]))])
    .sort((a, b) => a[1] - b[1])
    .slice(0, 10);

  return Object.fromEntries(bowlerRuns);
}


module.exports = {
  matchPlayed,
  matchWonbyTeam,
  extraRunsbyPerTeam,
  economyBowler,
};
